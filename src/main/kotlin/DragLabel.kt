import java.awt.Point
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionListener
import javax.swing.ImageIcon
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.SwingUtilities
import javax.swing.event.MouseInputListener
import kotlin.math.abs

class DragLabel(icon: ImageIcon, private val frame: JPanel, private val map: SpatialMap, val path: String) : JLabel(icon), MouseMotionListener, MouseInputListener {
    companion object {
        const val tileSize = 64
        const val adjust = 128 - tileSize
    }
    init {
        addMouseMotionListener(this)
        addMouseListener(this)
    }

    override fun mouseClicked(e: MouseEvent?) {

    }

    override fun mousePressed(e: MouseEvent?) {

    }

    override fun mouseReleased(e: MouseEvent?) {
        val (row, col) = map.getIndex(this.location)
        this.location = Point(col*tileSize, abs(row*tileSize - (ImagePanel.height- tileSize)))
    }

    override fun mouseEntered(e: MouseEvent?) {

    }

    override fun mouseExited(e: MouseEvent?) {

    }

    override fun mouseDragged(e: MouseEvent?) {
        val p = SwingUtilities.convertPoint(e!!.component, e.point, frame)
        this.location = Point(p.x, p.y)
    }

    override fun mouseMoved(e: MouseEvent?) {
    }
}