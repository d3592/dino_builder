import java.awt.Color
import java.awt.Graphics
import java.awt.Image
import javax.swing.JPanel
import kotlin.collections.ArrayList

class ImagePanel(layout: DragLayout, map: SpatialMap): JPanel(layout) {
    companion object {
        const val width = 960
        const val height = 540
    }
    var bg: Image? = null
    private val lines: ArrayList<Line> = ArrayList()

    init {

        for (x in 0..map.numRows.toInt()) {
            if (x * DragLabel.tileSize < ImagePanel.height) {
                lines.add(Line(ImagePanel.width, 0, ImagePanel.height - x*DragLabel.tileSize, ImagePanel.height - x*DragLabel.tileSize))
            }
        }
        for (y in 0..map.numCols.toInt()) {
            lines.add(Line(y*DragLabel.tileSize,y*DragLabel.tileSize, ImagePanel.height, 0))
        }
    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        if (bg != null) {
            g!!.drawImage(bg, 0, 0 ,this)
        }
        if (MainFrame.gridShow) {
            if (g != null) {
                lines.forEach{
                    it.paint(g)
                }
            }
        } else {
            g!!.clearRect(0, 0, width, height)
        }
    }

    class Line(val x1: Int, val x2: Int, val y1: Int, val y2: Int) {
        fun paint(g: Graphics) {
            g.color = Color.WHITE
            g.drawLine(x1, y1, x2, y2)
        }
    }
}