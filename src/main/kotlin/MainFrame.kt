import org.json.JSONArray
import org.json.JSONObject
import utils.LoadType
import java.awt.Dimension
import java.awt.Image
import java.awt.Toolkit
import java.io.File
import javax.swing.*
import javax.swing.filechooser.FileNameExtensionFilter
import kotlin.math.abs;

class MainFrame: JFrame() {
    companion object {
        var gridShow = true
    }
    private val tileFiles = ArrayList<List<File>>()
    private val jListTiles = ArrayList<JList<ImageIcon>>()
    private val map = SpatialMap(ImagePanel.height.toDouble(), ImagePanel.width.toDouble(), DragLabel.tileSize.toDouble())
    private val zone = ImagePanel(DragLayout(), map)
    private val tileList = ArrayList<DragLabel>()
    private val insertButton = JButton("Add")
    private val tabPane = JTabbedPane(JTabbedPane.TOP)
    init {
        addMenu()
        setListeners()
        create()
    }

    private fun create() {
        val configPath = this.javaClass.getResource("config.json")?.path
        if (configPath != null) {
            val configJson = JSONObject(File(configPath).readText())
            val assets = try { configJson.getString("assets") } catch (e: Exception) { null }
            val background = try { configJson.getString("background") } catch (e: Exception) { null }
            if (assets != null) loadAssets(File(assets))
            if (background != null) loadBackground(File(background))
        }
        zone.add(insertButton, JPanel.TOP_ALIGNMENT)
        val split = JSplitPane(JSplitPane.HORIZONTAL_SPLIT, zone, tabPane)
        split.dividerLocation = ImagePanel.width + 5
        add(split)
        title = "Dragon Builder"
        defaultCloseOperation = EXIT_ON_CLOSE
        setLocationRelativeTo(null)
        size = Dimension(1000, 600)

    }

    private fun setListeners() {
        insertButton.addActionListener{
            val image = ImageIcon(Toolkit.getDefaultToolkit()
                .createImage(tileFiles[tabPane.selectedIndex][jListTiles[tabPane.selectedIndex].selectedIndex].absolutePath)
                .getScaledInstance(DragLabel.tileSize, DragLabel.tileSize, Image.SCALE_SMOOTH))
            val icon = DragLabel(image, zone, map, tileFiles[tabPane.selectedIndex][jListTiles[tabPane.selectedIndex].selectedIndex].absolutePath)
            tileList.add(icon)
            zone.add(icon)
            revalidate()
        }
    }

    private fun addMenu() {
        val bar = JMenuBar()
        val file = JMenu("File")
        val load = JMenu("Load")
        val loadBg = JMenuItem("Load Background")
        val loadAssets = JMenuItem("Load Assets")
        val save = JMenuItem("Save")
        val export = JMenuItem("Export")
        val grid = JMenuItem("Grid")

        loadAssets.addActionListener{ openLoadDialog(LoadType.ASSET) }
        loadBg.addActionListener{ openLoadDialog(LoadType.BACKGROUND) }
        export.addActionListener{ exportJson() }
        grid.addActionListener{ gridShow = !gridShow }

        load.add(loadBg)
        load.add(loadAssets)

        file.add(load)
        file.add(save)
        file.add(export)
        file.add(grid)
        bar.add(file)
        jMenuBar = bar
    }

    private fun openLoadDialog(type: LoadType) {
        val loadDialog = JDialog(this)
        val fileSearch = JFileChooser()

        when (type) {
            LoadType.ASSET -> {
                fileSearch.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
                fileSearch.addActionListener{
                    val f = fileSearch.selectedFile
                    loadAssets(f)
                    loadDialog.dispose()
                }
            }
            LoadType.BACKGROUND -> {
                fileSearch.fileFilter = FileNameExtensionFilter("only png", "png")
                fileSearch.addActionListener{
                    val f = fileSearch.selectedFile
                    loadBackground(f)
                    loadDialog.dispose()
                }
            }
        }

        loadDialog.add(fileSearch)
        loadDialog.setLocationRelativeTo(this)
        loadDialog.pack()
        loadDialog.isVisible = true
    }

    private fun loadBackground(file: File) {
        saveConfig("background", file.absolutePath)
        zone.bg = Toolkit.getDefaultToolkit().createImage(file.absolutePath)
            .getScaledInstance(ImagePanel.width, ImagePanel.height, Image.SCALE_SMOOTH)
    }

    private fun loadAssets(file: File) {
        saveConfig("assets", file.absolutePath)
        val assets = file.listFiles()
        if (assets != null) {
            assets.forEach {
                if (it.isDirectory) {
                    val innerFiles = it.listFiles()
                    if (innerFiles != null) {
                        val icons = innerFiles.map { f ->
                            // todo refactor to just recurse through all dirs
                            // todo for now, just going to assume that all files under one nested dir
                            ImageIcon(f.absolutePath)
                        }
                        val list = JList(icons.toTypedArray())
                        tileFiles.add(innerFiles.toList())
                        jListTiles.add(list)
                        list.selectionMode = ListSelectionModel.SINGLE_SELECTION

                        val pane = JScrollPane(list)

                        tabPane.addTab(it.name, pane)
                    }
                }
            }
        } else {
            showAlert("Unable to load assets!")
        }
    }

    private fun showAlert(text: String) {
        val alert = JDialog(this)
        alert.add(JTextArea(text))
        alert.isVisible = true
    }

    private fun exportJson() {
        val loadDialog = JDialog(this)
        val fileSearch = JFileChooser()
        fileSearch.dialogType = JFileChooser.SAVE_DIALOG

        fileSearch.addActionListener{
            val json = JSONArray()

            tileList.forEach {
                val element = JSONObject()
                element.put("x", ((it.x/DragLabel.tileSize) * 128))
                element.put("y", (abs(it.y - (ImagePanel.height - DragLabel.tileSize))/DragLabel.tileSize) * 128)
                element.put("tile", it.path)
                json.put(element)
            }
            val file = File("${fileSearch.selectedFile.absolutePath}.json")
            file.writeText(json.toString())
            loadDialog.dispose()
        }

        loadDialog.add(fileSearch)
        loadDialog.setLocationRelativeTo(this)
        loadDialog.pack()
        loadDialog.isVisible = true

    }

    private fun saveConfig(key: String, value: String) {
        val configPath = this.javaClass.getResource("config.json")?.path
        val json = if (configPath != null) {
            JSONObject(File(configPath).readText())
        } else {
            JSONObject()
        }
        json.put(key, value)

        val configFile = if (configPath != null) {
            File(configPath)
        } else {
            File("${this.javaClass.getResource("/").path}/config.json")
        }
        configFile.writeText(json.toString())
    }
}