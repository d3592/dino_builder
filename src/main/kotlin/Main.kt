import java.awt.EventQueue

private fun createGui() {
    val frame = MainFrame()
    frame.isVisible = true
}

fun main() {
    EventQueue.invokeLater(::createGui)
}