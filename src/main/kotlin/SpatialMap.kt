import java.awt.Point
import kotlin.math.ceil
import kotlin.math.abs

class SpatialMap(height: Double, width: Double, cellSize: Double) {
    val conversionFactor = 1.0/cellSize
    val numRows = ceil(height/cellSize)
    val numCols = ceil(width/cellSize)
    val multiplier = if (numCols > numRows) {
        numCols
    } else numRows

    fun getIndex(p: Point): Pair<Int, Int> {
        //val index = (p.x * conversionFactor).toInt() + (p.y * conversionFactor * multiplier).toInt()
        val row = abs(ImagePanel.height - p.y)/DragLabel.tileSize
        val col = (p.x / DragLabel.tileSize)
        return Pair(row, col);
    }

}